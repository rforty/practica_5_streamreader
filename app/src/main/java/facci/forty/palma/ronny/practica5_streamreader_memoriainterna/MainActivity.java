package facci.forty.palma.ronny.practica5_streamreader_memoriainterna;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btn_prefer, btn_nombres;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn_prefer= findViewById(R.id.btn_preferencias);
        btn_nombres=findViewById(R.id.btn_nombres);

        btn_prefer.setOnClickListener(this);
        btn_nombres.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.btn_nombres :
                Intent intent1 = new Intent(MainActivity.this, MemoriaInterna.class);
                startActivity(intent1);

                break;

            case R.id.btn_preferencias :
                Intent intent2 = new Intent (MainActivity.this, GuardarPreferencias.class);
                startActivity(intent2);
                break;
        }
    }
}

