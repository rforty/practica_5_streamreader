package facci.forty.palma.ronny.practica5_streamreader_memoriainterna;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class GuardarPreferencias extends AppCompatActivity {

    CheckBox ch1, ch2, ch3;
    RadioGroup rad;
    EditText ed;
    Button btn;
    TextView cajaDatos;
    Button btn2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guardar_preferencias);

        assert getSupportActionBar() != null;

        getSupportActionBar().setTitle("Configurar Preferencias");

        ch1 = findViewById(R.id.checkBox1);
        ch2 = findViewById(R.id.checkBox2);
        ch3 = findViewById(R.id.checkBox3);
        rad = findViewById(R.id.radio);
        ed = findViewById(R.id.edit1);
        btn = findViewById(R.id.buttonGuardarCambios);
        cajaDatos = findViewById(R.id.txt_datos);
        btn2 = findViewById(R.id.button_leer_prefer);
        String[] archivos = fileList();

        if (existe(archivos, "config.txt")) {
            try {
                BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("config.txt")));
                String datos = lector.readLine();
                String[] listaPersonas = datos.split(";");
                ed.setText("");
                for (int i = 0; i < listaPersonas.length; i++) {
                    if (listaPersonas[i].equals("Sans")) {
                        ch1.setChecked(true);
                    }
                    if (listaPersonas[i].equals("Asap")) {
                        ch2.setChecked(true);
                    }
                    if (listaPersonas[i].equals("Monserrat")) {
                        ch3.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn1")) {
                        RadioButton r = findViewById(R.id.rbtn1);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn2")) {
                        RadioButton r = findViewById(R.id.rbtn2);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn3")) {
                        RadioButton r = findViewById(R.id.rbtn3);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i].equals("R.id.rbtn4")) {
                        RadioButton r = findViewById(R.id.rbtn4);
                        r.setChecked(true);
                    }
                    if (listaPersonas[i+1].equals("AVER")) {
                        ed.setText(listaPersonas[i]);
                    }
                }
                lector.close();
            } catch (Exception ex) {
                ex.printStackTrace();
                Log.e("ARCHIVO MI", "Error en la lectura del archivo " + ex.getMessage());
            }
        }


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    deleteFile("config.txt");
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("config.txt", Activity.MODE_APPEND));
                    if (ch1.isChecked()) {
                        escritor.write("Comic-Sans;");
                    }
                    if (ch2.isChecked()) {
                        escritor.write("Times New Roman;");
                    }
                    if (ch3.isChecked()) {
                        escritor.write("Arial;");
                    }
                    switch (rad.getCheckedRadioButtonId()) {
                        case R.id.rbtn1:
                            escritor.write("Negro;");
                            break;
                        case R.id.rbtn2:
                            escritor.write("Marron;");
                            break;
                        case R.id.rbtn3:
                            escritor.write("Verde;");
                            break;
                        case R.id.rbtn4:
                            escritor.write("Azul;");
                            break;
                    }
                    escritor.write(ed.getText().toString() + ";");
                    escritor.flush();
                    escritor.close();
                    Toast.makeText(view.getContext(), "Los datos se grabaron con éxito", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("ARCHIVO MI", "Error en el archivo de escritura");
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("config.txt")));
                    String datos = lector.readLine();
                    String[] listaPersonas = datos.split(";");
                    cajaDatos.setText("");
                    for (int i = 0; i < listaPersonas.length; i++) {
                        cajaDatos.append(listaPersonas[i] + "\n ");
                    }
                    lector.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Log.e("ARCHIVO MI", "Error en la lectura del archivo " + ex.getMessage());
                }
            }
        });
    }

    private boolean existe(String[] archivos, String archbusca) {
        for (int f = 0; f < archivos.length; f++)
            if (archbusca.equals(archivos[f]))
                return true;
        return false;
    }
}
